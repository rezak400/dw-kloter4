var input = prompt("Masukkan Panjang lebar/tinggi : ");

function cetak_gambar(angka){
    if (angka % 2 == 0) {
    
        console.log("+ ".repeat(angka));
        
        for (let index = 0; index < angka-2; index++) {
            // var output = "= ".repeat(angka);
            // console.log(output)
            var output = "";
    
            for(let a = 1; a<=angka; a++){
                if(a % 3 == 0){
                    output += "+ "
                }else{
                    output += "= "
                }
            }
            console.log(output);
        }
        console.log("+ ".repeat(angka));
    
    }else {
        console.log("MASUKKAN ANGKA GENAP SAJA")
    }
    
}

console.log(cetak_gambar(input));
console.log("Silahkan jalankan fungsi cetak_gambar(panjang) contoh : cetak_gambar(8)")