-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 04, 2020 at 02:57 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sepeda`
--
CREATE DATABASE IF NOT EXISTS `sepeda` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sepeda`;

-- --------------------------------------------------------

--
-- Table structure for table `importir_tb`
--

DROP TABLE IF EXISTS `importir_tb`;
CREATE TABLE `importir_tb` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `importir_tb`
--

INSERT INTO `importir_tb` (`id`, `name`, `address`, `phone`) VALUES
(1, 'PT.Dumbways', 'Samping tetangga', '080000000'),
(2, 'PT.Miracle', 'Depannya tetangga', '0890000000'),
(3, 'PT.RMP', 'Deket Tetangga', '081000000'),
(4, 'PT.Sejahtera', 'Belakang Tetangga', '082000000');

-- --------------------------------------------------------

--
-- Table structure for table `produk_tb`
--

DROP TABLE IF EXISTS `produk_tb`;
CREATE TABLE `produk_tb` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `importir_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_tb`
--

INSERT INTO `produk_tb` (`id`, `name`, `importir_id`, `photo`, `qty`, `price`) VALUES
(1, 'Brompton', 1, 'brompton.jpg', 100, 2500000),
(2, 'Fixie', 2, 'Fixie.jpg', 145, 3000000),
(3, 'Polygon', 1, 'polygon.jpg', 150, 1600000),
(4, 'Element', 1, 'element.jpg', 50, 1600000),
(28, 'baruy', 1, 'sepeda.jpg', 213, 123);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `importir_tb`
--
ALTER TABLE `importir_tb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk_tb`
--
ALTER TABLE `produk_tb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produk_importir` (`importir_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `importir_tb`
--
ALTER TABLE `importir_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `produk_tb`
--
ALTER TABLE `produk_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `produk_tb`
--
ALTER TABLE `produk_tb`
  ADD CONSTRAINT `fk_produk_importir` FOREIGN KEY (`importir_id`) REFERENCES `importir_tb` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
